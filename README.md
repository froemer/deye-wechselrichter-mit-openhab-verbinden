# Integration des Deye über Fritzbox in OpenHAB

## Immer sinnvoll
Mit der bei mir ausgelieferten Firmware gab es noch Sicherheitsbedenken. Upgrade und neue Firmware ist hier beschrieben: 
https://www.oekostromhelden.de/wp-content/themes/oekostromhelden/datasheets/Anleitung%20zum%20Firmware%20Update%20DEYE.pdf

## Diskussion der Alternativen
Es gibt verschieden leistungsfähige Möglichkeiten auf den Deye zuzugreifen. 
- Der Wechselrichter verbindet sich mit einer Cloud, aus der man die Daten abgreifen kann.
    - das hat den Nachteil, dass man die Cloud mit im Spiel hat, die allerdings auch
    - dafür sorgt, dass z.B. das Datum korrekt gestellt wird.
- Es gibt ein sehr tiefgreifendes Tool, das viele Daten aus dem Deye zieht und sogar schreibend zugreifen kann. Aber man macht es auf eigene Gefahr. https://github.com/kbialek/deye-inverter-mqtt
    - Das ist sehr umfangreich und war mir zu groß um ungefährlich eingesetzt zu werden
- Der Deye stellt eine Webseite zur Verfügung, die ein limitiertes Set von Informationen anbietet. Insbesondere:
    - die derzeitige produzierte Leistung in Watt
    - die Gesamtleistung in kWh bisher

Die Webseite stellt alle Daten zur Verfügung die ich brauche. Allerdings steht diese nur zur Verfügung, wenn die Sonne scheint.

Hier wird diskutiert, wie man die letzte Möglichkeit nutzen kann
https://community.openhab.org/t/deye-sun600g3-microinverter-status-query-via-curl/144912
. Diesen Weg werde ich gehen.

## Anmelden in der Fritzbox

Ich wollte meine Wechselrichter nicht mit Servern sprechen lassen, die ich nicht kontrolliere. Das käme für mich nur über den Gastzugang in Frage. Auf eine Komponente mit Gastzugang kann ich aber über mein Netzwerk nicht mehr zugreifen. Ich nehme die Wechselrichter in mein Heimnetz, präpariere die Fritzbox aber so, dass der Wechselrichter von vornherein nicht nach draußen telefonieren darf.

Ruft man die Webseite des Deye auf, liefert dieser in den "Device information" zwei MAC-Adressen. Uns interessiert die Mac-Adresse des STA-mode. Der AP mode ist der Access Point. 

In der Fritzbox unter Heimnetz -> Netzwerk findet sich ganz unten `Gerät hinzufügen`. Tragen sie dort Namen des Gerätes und die MAC Adresse ein und bestätigen sie. In der Liste den ungenutzten Verbindung taucht dann diese Verbindung auf. Diese können wir editieren und den Internetzugang sperren.

## Anmelden des Deye im eigenen WLan
Entsprechend der Dokumentation über die Webseite des Deye kann man sich jetzt mittels des Wizard an der eigenen Fritzbox anmelden. Die Fritzbox wird die Anmeldung auf die vordefinierten MAC-Adressen mappen und der Verbindung die Kindersicherung aufsetzen.

Damit ist nicht ausgeschlossen, wenn jemand die Befürchtung hat, dass aus der aktuellen Firmware etwas mein Netz angreifen könnte. Über die Profile in der Fritzbox sollte man die Kommunikation weiter auf das Notwendige einschränken. 

>**Warnung**
>Wenn man hier tiefer als Kindersicherung einsteigen will, sollte man jeden Fall genau wissen, was man da tut. Eine Kommunikation der Wechselrichter mit deren Heimatcloud sollte aber mit Kindersicherung bereits ausgeschlossen sein und damit das Risiko begrenzen. Ziehen sie Dokumentation und Support der verwendeten Fritzbox zu Rate, wenn sie an dieser Änderungen durchführen.

## Curlscript
Den Curlbefehl mit Verbindung mit grep [aus der Diskussion oben](https://community.openhab.org/t/deye-sun600g3-microinverter-status-query-via-curl/144912) oben fand ich sehr schön für die weitere Bearbeitung. Den habe ich als Script so in `openhab/conf/scripts/curl_Wechselrichter.sh` eingetragen. Das Script erwartet die IP oder Hostnamen des Wechselrichters. Username und Password sind diejenigen, die sie im Wizard als Anmeldedaten für die Webseite hinterlegt haben. Wenn für die Wechselrichter unterschiedliche Anmeldedaten verwendet werden sollen, muss man entsprechend parametrisieren.
```
#!/bin/bash
timeout 2 curl -su username:password $1/status.html | grep "$dat = "
```
Wenn man in OpenHAB ein Script via executeCommandLine aufruft, definiert man auch eine Dauer, nach der abgebrochen werden soll. Das wird hier insbesondere immer dann passieren, wenn die Sonne nicht mehr scheint und so der Wechselrichter nicht mehr antwortet. Wird das Script abgebrochen, bekommt man eine Warnung, für die ich keine Möglichkeit gefunden habe, diese abzufangen. Alternativ verwende ich hier `timeout` welches nach gegebenen Sekunden VORHER abbrechen soll. Auf Stdout kommt dann "Terminated" an. DAS können wir abfangen. Lieber wäre mir eine ordentliche Fehlerbehandlung an EINER Stelle. Ich bin für Vorschläge offen.

## Items in OpenHAB
```
Number	Balkonkraftwerk_CurrentW    "Balkonkraftwerk Current [%.2f]"	<gauge>	(gPers)
Number	Balkonkraftwerk_OverallkWh  "Balkonkraftwerk Overall [%.2f]"	<gauge>	(gPers)

Number	RechterWR_CurrentW          "Rechter Wechselrichter Current [%.2f]"	<gauge>	(gPers)
Number	RechterWR_OverallkWh        "Rechter Wechselrichter Overall [%.2f]"	<gauge>	(gPers)

Number	LinkerWR_CurrentW           "Linker Wechselrichter Current [%.2f]"	<gauge>	(gPers)
Number	LinkerWR_OverallkWh         "Linker Wechselrichter Overall [%.2f]"	<gauge>	(gPers)
```
Die Items sind hier nur schlichte Variablen. Bei mir trägt die Zugehörigkeit zur Gruppe `gPers` dafür Sorge, dass die Daten persistiert werden. 

## Datenübernahme in OpenHAB

### eine Function kontrolliert das Script
So sieht dann das Verdoppeln über den linken und rechten Wechselrichter etwas übersichtlicher aus:

```
val Procedures$Procedure2<NumberItem, Number> update = [ myNumberItem, myNumber |
  if ((myNumberItem.state as Number).doubleValue() != myNumber) {  
    myNumberItem.postUpdate(myNumber) 
  }
]

val Functions$Function4<String, NumberItem,  NumberItem,  Procedures$Procedure2<NumberItem, Number>, Number> 
   getWebData2Items = [ wR_IP,  itemCurrent, itemOverall, update |
    
    var int returnValue = 0                                  
    var String webData = executeCommandLine(Duration.ofSeconds(3), "/bin/bash", "/openhab/conf/scripts/curl_Wechselrichter.sh", wR_IP)
                                                             //3 muss höher sein als timeout im Script!!

    try { 
      if (webData === null) { // -------------------------------- Keine Daten bekommen (sollte eigentlich wenigstens "Terminated" sein)
        returnValue = 3
      
      } else { if (webData.equals("Terminated")) { // ----------- für uns logisch gleichbedeutend zu "null"
        returnValue = 3

      } else { if (webData.indexOf("webdata_now_p") < 200) { // - Wir haben eine leere Seite bekommen (Wolke?)
        returnValue = 2

      } else { // ----------------------------------------------- Korrekte Daten bekommen
          var int next = webData.indexOf(";",261)
          update.apply(itemCurrent, Integer::parseInt( webData.substring(261,next-1)))

          if (( itemCurrent.state as Number).intValue() > 0) { // Wenn die Leistung zu gering ist, ist overall unzuverlässig
            update.apply(itemOverall, Float::parseFloat( webData.substring(next+26,webData.indexOf(";",next+1)-1)))

          }

      } } } 
    } catch(Throwable t) {
      logError("Stati_Werte_Balkonkraftwerk getData", "Parsing error on: <" + webData + ">")
    }
    returnValue
]
```

### Die Regel aggregiert die Daten
und wird verwendet um ein wenig Fehlerbehandlung aufzunehmen:
```
var int anzahlAusfaelle = 0

rule "Stati_Werte_Balkonkraftwerk getData"
when
    Time cron "0 */1  * * * ?" // zum Start jeder Minute
then
    var int rechts = getWebData2Items.apply("Rechter-WR", RechterWR_CurrentW, RechterWR_OverallkWh, update)
    var int links  = getWebData2Items.apply("Linker-WR",  LinkerWR_CurrentW,  LinkerWR_OverallkWh,  update)

    if (rechts + links <= 3) { // wenigstens einer hat Antwort gegeben
      update.apply(Balkonkraftwerk_CurrentW,   (RechterWR_CurrentW.state as Number) +   (LinkerWR_CurrentW.state as Number) )
      update.apply(Balkonkraftwerk_OverallkWh, (RechterWR_OverallkWh.state as Number) + (LinkerWR_OverallkWh.state as Number) )
      anzahlAusfaelle = 0

    } else {
      if (anzahlAusfaelle == 2) { // d.h. wir hatten schon den zwei Fehlversuche, wir loggen das und setzen auf Null (einmal)
        logInfo("Stati_Werte_Balkonkraftwerk getData", "Keine Meldung seid 3 Updates; Current auf Null")
        update.apply(RechterWR_CurrentW, 0) 
        update.apply(LinkerWR_CurrentW, 0) 
        update.apply(Balkonkraftwerk_CurrentW, 0)

      }
      anzahlAusfaelle = anzahlAusfaelle + 1 // bis die Sonne wieder scheint oder wir auch hier eine Unterscheidung machen

    }
end
```
### Mittels Grafana kann dann das Ergebnis so aussehen
![Grafana](/grafana.png)

Das entspricht dem, was ich haben wollte und kann auf die Werte nun Logik aufsetzen um damit zu arbeiten.

## Probleme mit dieser Lösung:

### keine tägliche Abgrenzung oder andere Leistungen aus der Cloud
Wie oben geschrieben, kennen die Wechselrichter kein aktuelles Datum, wenn diese nicht in ihre Cloud telefonieren dürfen. Der "Overall"-Wert, den ich hier betrachte, ist an sich der "Daily", der aber nie auf Null gesetzt wird. Nur dieser hat eine Nachkommastelle, die für die tägliche Betrachtung dann doch nützlich ist. Für mich ist das kein wirkliches Problem, aber sicher Wert hier noch einmal erwähnt zu werden.
So wären beispielsweise nach meinem Kenntnisstand auch automatisierte Updates ausgeschlossen. 

### zeitlicher Versatz
Wenn man die Werte der Wechselrichter vergleicht, sieht man, dass mal der eine oder andere "hinterherhinkt". Man könnte jetzt sicher noch versuchen durch geeignete Fehlerlogik auf den Daten selbst die Kurve besser zu glätten. Das wäre sicher eine interessante statistische Fingerübung, aber praktisch wohl nicht wirklich relevant. Die Addition in das "Balkonkraftwerk" hat schon einen ähnlichen Effekt.

### Ohne Sonne ausschalten
Ich werde noch eine Variable anbauen, mittels derer ich die Ausführung zwischen Sonnenuntergang und Sonnenaufgang ganz unterbinde. Das würde den Code aber hier nur unnötig verkomplizieren.
